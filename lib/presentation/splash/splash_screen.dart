import 'package:catapp/bloc/app_bloc/app_bloc.dart';
import 'package:catapp/bloc/app_bloc/app_event.dart';
import 'package:catapp/bloc/splash_bloc/splash_bloc.dart';
import 'package:catapp/bloc/splash_bloc/splash_state.dart';
import 'package:catapp/common/resources/colors.dart';
import 'package:catapp/common/resources/images.dart';
import 'package:catapp/common/routes/routs.dart';
import 'package:catapp/common/utils/dimension/dimension_util.dart';
import 'package:catapp/common/utils/logger/logger.dart';
import 'package:catapp/common/utils/navigation/navigation.dart';
import 'package:catapp/di/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SplashState();
}

class SplashState<SplashScreen> extends State {
  static final String tag = "SplashState";

  @override
  void initState() {
    super.initState();
    log('$tag initState');

    getIt.get<AppBloc>().add(LoadUserEvent());
  }

  @override
  void dispose() {
    log('$tag dispose');

    getIt.get<SplashBloc>().close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    DimensionUtil.init(context, false, 380.0);

    return Scaffold(
      body: BlocConsumer(
        bloc: getIt.get<SplashBloc>(),
        listener: (context, state) {
          log('$tag listener state changed $state');
          if (state is SplashFinishedState) {
            if (state.authorized) {
              _navigateToHomeScreen();
            } else {
              _navigateToLoginScreen();
            }
          }
        },
        builder: (context, state) {
          log('$tag builder state changed $state');
          return Container(
            color: AppColors.white,
            child: Center(
              child: Image.asset(
                AppImages.splash,
                width: DimensionUtil.horizontalFraction(0.7),
                height: DimensionUtil.horizontalFraction(0.7),
              ),
            ),
          );
        },
      ),
    );
  }

  void _navigateToHomeScreen() {
    Navigation.pushNamedAndRemoveUntil(AppRoutes.home, (_) => false);
  }

  void _navigateToLoginScreen() {
    Navigation.pushNamedAndRemoveUntil(AppRoutes.login, (_) => false);
  }
}
