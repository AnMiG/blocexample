import 'package:catapp/common/routes/routs.dart';
import 'package:catapp/presentation/splash/splash_screen.dart';
import 'package:flutter/material.dart';

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

class AppRouter {
  static MaterialPageRoute onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(builder: (context) {
      switch (settings.name) {
        case AppRoutes.splash:
          return SplashScreen();
        case AppRoutes.login:
        // return LoginScreen(settings);
        case AppRoutes.home:
        // return HomeScreen(settings);
        default:
          return getHome();
      }
    });
  }

  static Widget getHome() {
    return SplashScreen();
  }
}
