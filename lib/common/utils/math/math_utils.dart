import 'dart:math' as math;

class MathUtils {
  static final double degrees90 = MathUtils.degreesToRadians(90.0);
  static final double degrees180 = MathUtils.degreesToRadians(180.0);
  static final double degrees270 = MathUtils.degreesToRadians(270.0);
  static final double degrees360 = MathUtils.degreesToRadians(360.0);

  static double degreesToRadians(double degrees) {
    return (degrees * math.pi) / 180.0;
  }

  ///Checks if the given object completely fits in the given container.
  ///returns 0 if object is completely visible inside his container.
  static double findObjectYOverlap(double containerY, double containerHeight,
      double objectY, double objectHeight) {
    double objectExtra = objectY + objectHeight;
    double containerExtra = containerY + containerHeight;
    if (objectHeight > containerHeight) {
      return objectY - containerY;
    }
    if (objectY < containerY) {
      return objectY - containerY; //minus
    }
    if (objectExtra > containerExtra) {
      return objectExtra - containerExtra; //plus
    }
    return 0;
  }

  ///Checks if the given object completely fits in the given container.
  ///returns 0 if object is completely visible inside his container.
  static double findObjectXOverlap(double containerX, double containerWidth,
      double objectX, double objectWidth) {
    double objectExtra = objectX + objectWidth;
    double containerExtra = containerX + containerWidth;
    if (objectWidth > containerWidth) {
      return objectX - containerX;
    }
    if (objectX < containerX) {
      return objectX - containerX; //minus
    }
    if (objectExtra > containerExtra) {
      return objectExtra - containerExtra; //plus
    }
    return 0;
  }
}
