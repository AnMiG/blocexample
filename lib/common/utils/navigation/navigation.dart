import 'package:flutter/material.dart';

class Navigation {
  static final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  static Future<Object?>? pushNamed(
    String routeName, {
    Object? arguments,
  }) {
    return navigatorKey.currentState
        ?.pushNamed(routeName, arguments: arguments);
  }

  static Future<Object?>? pushNamedAndRemoveUntil(
    String newRouteName,
    RoutePredicate predicate, {
    Object? arguments,
  }) {
    return navigatorKey.currentState?.pushNamedAndRemoveUntil(
        newRouteName, predicate,
        arguments: arguments);
  }

  static void pop() {
    navigatorKey.currentState?.pop();
  }

  BuildContext? getContext() {
    return navigatorKey.currentState?.context;
  }
}
