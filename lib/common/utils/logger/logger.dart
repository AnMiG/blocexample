import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

int _last = 0;

void log(Object message) {
  var now = new DateTime.now();
  var formatter = new DateFormat('HH:mm:ss.SSS');
  String time = formatter.format(now);
  String m = message.toString();
  String elapsed = "";
  if (_last != 0) {
    int time = now.millisecondsSinceEpoch - _last;
    if (time > 1000) {
      elapsed = '${(time / 1000).floor()}.${(time % 1000).toInt()}';
    } else {
      elapsed = time.toString();
    }
  }
  _last = now.millisecondsSinceEpoch;
  debugPrint('$time | $elapsed |  | $m');
}

// void logRequest(
//     String name, String url, Map<String, String>? headers, dynamic body) {
//   log('$name $url');
//   if (headers != null) {
//     log('Headers:');
//     headers.forEach((key, val) => log('$key:$val\n'));
//   }
//   if (body != null) {
//     if (body is Map) {
//       StringBuffer str = StringBuffer('body: {');
//       body.forEach((k, v) => str.write('${k.toString()}: ${v.toString()}, '));
//       str.write('}');
//       log(str);
//     } else if (body is List<int>) {
//       log('body: bytes array length: ${body.length}');
//     } else if (body is String) {
//       log('body: $body');
//     }
//   }
// }

// void logResponse(String name, String url, Response? response) {
//   if (response == null) {
//     log('$name $url\nResponse NULL');
//     return;
//   }
//   log('$name ${response.statusCode} ${response.reasonPhrase} $url');
//   if (response.headers != null) {
//     log('Headers:');
//     response.headers.forEach((key, val) => log('$key:$val'));
//   }
//   log('Body:');
//   log('${response.body}');
// }
