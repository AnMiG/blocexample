import 'package:flutter/cupertino.dart';

class DimensionUtil {
  static bool _initialized = false;
  static double _planWidth = 0;
  static double _planHeight = 0;
  static double _screenWidth = 0;
  static double _screenHeight = 0;
  static double _planFactRatio = 0;

  ///ATTENTION! Use this only in orientation fixed apps, eg: only landscape or only
  ///portrait. If orientation in your app will be changed - find another solution.
  ///
  static void init(BuildContext context, bool isLandscape, double planSize) {
    if (_initialized) return;
    _initialized = true;
    if (isLandscape) {
      double w = MediaQuery.of(context).size.width;
      double h = MediaQuery.of(context).size.height;
      if (h > w) {
        w = MediaQuery.of(context).size.height;
        h = MediaQuery.of(context).size.width;
      }
      DimensionUtil.initWithPlanResoluton(
          screenWidth: w,
          screenHeight: h,
          isLandscape: isLandscape,
          planHeight: planSize);
    } else {
      double w = MediaQuery.of(context).size.width;
      double h = MediaQuery.of(context).size.height;
      if (w > h) {
        w = MediaQuery.of(context).size.height;
        h = MediaQuery.of(context).size.width;
      }
      DimensionUtil.initWithPlanResoluton(
          screenWidth: w,
          screenHeight: h,
          isLandscape: isLandscape,
          planWidth: planSize);
    }
  }

  //=================== S E R V I C E == F U N C T I O N S =====================

  ///ATTENTION! Use this only in orientation fixed apps, eg: only landscape or only
  ///portrait. If orientation in your app will be changed - find another solution.
  ///
  ///Initializes the app dimens manager. "Planned" values - is the values from your
  ///planned layout, eg: dp from Zeplin or px or points from Invision or Figma.
  ///No matter if the planned dimen is sp, px, dp, cm, inch or any other metric.
  ///The single goal is that the planned points should be of the same metric system
  ///in whole app.
  ///Pass the screen actual width and height in flutter logical points. Then pass
  ///if the app is landscape or not.
  ///Check the planned resolution from your template layout.
  /// - For example: on Zeplin the design is made as 768x1280 points for phones.
  ///Pass the planWidth=768. The planHeight will be calculated by the program, depending
  ///on the device sides ratio.
  /// - For example: on Zeplin the design is made as 2048x1536 points for tablets.
  ///Pass the planHeight=1536. The planWidth will be calculated by the program, depending
  ///on the device sides ratio.
  ///When you want to create the button with size 200x80 points on Zeplin template,
  ///you should call AppDimens.planToFact(200.0) and AppDimens.planToFact(80.0) accordingly to get the
  ///real button size.
  static void initWithPlanResoluton({
    required double screenWidth,
    required double screenHeight,
    required bool isLandscape,
    double planWidth = 0,
    double planHeight = 0,
  }) {
    if (planWidth != 0) {
      if (planHeight != 0) {
        if (isLandscape) {
          initWithPlanWidth(screenWidth, screenHeight, planWidth);
        } else {
          initWithPlanHeight(screenWidth, screenHeight, planHeight);
        }
      } else {
        initWithPlanWidth(screenWidth, screenHeight, planWidth);
      }
    } else if (planHeight != 0) {
      initWithPlanHeight(screenWidth, screenHeight, planHeight);
    } else {
      throw new Exception(
          "AppDimens.initWithPlanResoluton() One of screenFixedWidth Or screenFixedHeight must be 0");
    }
  }

  static void initWithPlanWidth(
      double pSscreenWidth, double pScreenHeight, double pPlanWidth) {
    _screenWidth = pSscreenWidth;
    _screenHeight = pScreenHeight;
    if (pPlanWidth != 0) {
      double ratio = _screenWidth / _screenHeight;
      _planWidth = pPlanWidth;
      _planHeight = pPlanWidth / ratio;
    } else {
      throw new Exception(
          "AppDimens.initWithPlanResoluton() The screenFixedWidth must be non 0");
    }
    _planFactRatio = _screenHeight / _planHeight;
  }

  static void initWithPlanHeight(
      double pSscreenWidth, double pScreenHeight, double pPlanHeight) {
    _screenWidth = pSscreenWidth;
    _screenHeight = pScreenHeight;
    if (pPlanHeight != 0) {
      double ratio = _screenWidth / _screenHeight;
      _planWidth = pPlanHeight * ratio;
      _planHeight = pPlanHeight;
    } else {
      throw new Exception(
          "AppDimens.initWithPlanResoluton() The screenFixedHeight must be non 0");
    }
    _planFactRatio = _screenHeight / _planHeight;
  }

  ///The screen width in Flutter logical pixels
  static double getFactWidth() {
    return _screenWidth;
  }

  ///The screen height in Flutter logical pixels
  static double getFactHeight() {
    return _screenHeight;
  }

  ///The screen width in abstract points from your design
  static double getPlanWidth() {
    return _planWidth;
  }

  ///The screen height in abstract points from your design
  static double getPlanHeight() {
    return _planHeight;
  }

  ///Converts the points from your design to Flutter logical points
  static double planToFact(double plan) {
    return plan * _planFactRatio;
  }

  ///Calculates the part of the screen height in Flutter logical pixels.
  ///ratio - 0..1
  static double verticalFraction(double ratio) {
    return _screenHeight * ratio;
  }

  ///Calculates the part of the screen width in Flutter logical pixels.
  ///ratio - 0..1
  static double horizontalFraction(double ratio) {
    return _screenWidth * ratio;
  }
}
