import 'package:flutter/material.dart';

/// To add a new strings run:
/// flutter pub run easy_localization:generate --source-dir assets/translations/ -f keys -o locale_keys.g.dart
const List<Locale> SUPPORTED_LOCALES = [
  Locale('en'),
  Locale('uk'),
];
const Locale DEFAULT_LOCALE = Locale('en');
const String TRANSLATIONS_PATH = 'assets/translations';
