import 'package:catapp/common/resources/resources_lib.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

ThemeData appTheme() => ThemeData(
      scaffoldBackgroundColor: AppColors.white,
      textTheme: appTextTheme,
      backgroundColor: AppColors.white,
      canvasColor: AppColors.white,
      appBarTheme: appBarTheme,
      dialogTheme: dialogTheme(),
      inputDecorationTheme: inputDecorationTheme(),
      cupertinoOverrideTheme: cupertinoThemeData,
      buttonTheme: buttonThemeData(),
      textSelectionTheme: TextSelectionThemeData(cursorColor: AppColors.black),
    );

const CupertinoThemeData cupertinoThemeData = CupertinoThemeData(
  primaryColor: AppColors.white,
);

ButtonThemeData buttonThemeData() => ButtonThemeData(
    height: 22.0,
    padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
    buttonColor: AppColors.blue,
    textTheme: ButtonTextTheme.primary,
    minWidth: 0.0,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.0)));

InputDecorationTheme inputDecorationTheme() => InputDecorationTheme(
      alignLabelWithHint: true,
      errorStyle: TextStyle(
        color: AppColors.red,
        fontSize: 14.0,
        fontFamily: AppFonts.robotoLight,
      ),
      labelStyle: TextStyle(
        color: AppColors.black,
        fontSize: 16.0,
        fontFamily: AppFonts.robotoRegular,
      ),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(width: 3.0, color: AppColors.blue),
      ),
      disabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(width: 3.0, color: AppColors.gray),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(width: 3.0, color: AppColors.blue),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(width: 3.0, color: AppColors.black),
      ),
      focusedErrorBorder: UnderlineInputBorder(
        borderSide: BorderSide(width: 3.0, color: AppColors.red),
      ),
      errorBorder: UnderlineInputBorder(
        borderSide: BorderSide(width: 3.0, color: AppColors.red),
      ),
    );

///Lok this: table https://material.io/design/typography/#type-scale
const TextTheme appTextTheme = TextTheme(
  headline5: const TextStyle(
    color: AppColors.black,
    fontSize: 34.0,
    fontFamily: AppFonts.robotoRegular,
  ),
  headline4: const TextStyle(
    color: AppColors.black,
    fontSize: 45.0,
    fontFamily: AppFonts.robotoRegular,
  ),
  headline3: TextStyle(
    color: AppColors.black,
    fontSize: 56.0,
    fontFamily: AppFonts.robotoLight,
  ),
  headline2: TextStyle(
    color: AppColors.black,
    fontSize: 112.0,
    fontFamily: AppFonts.robotoLight,
  ),
  headline1: TextStyle(
    color: AppColors.black,
    fontSize: 140.0,
    fontFamily: AppFonts.robotoLight,
  ),
  caption: TextStyle(
    color: AppColors.black,
    fontSize: 12.0,
    fontFamily: AppFonts.robotoRegular,
  ),
  button: TextStyle(
    color: AppColors.black,
    fontSize: 12.0,
    fontFamily: AppFonts.robotoBold,
  ),
  overline: TextStyle(
    color: AppColors.black,
    fontSize: 10.0,
    fontFamily: AppFonts.robotoRegular,
  ),
);

DialogTheme dialogTheme() => DialogTheme(
      backgroundColor: AppColors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
    );

const AppBarTheme appBarTheme = AppBarTheme(
  brightness: Brightness.light,
  color: AppColors.white,
  elevation: 1,
  iconTheme: IconThemeData(
    color: AppColors.black,
  ),
  textTheme: appTextTheme,
);
