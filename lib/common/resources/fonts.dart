class AppFonts {
  static const robotoBold = "RobotoBold";
  static const robotoRegular = "RobotoRegular";
  static const robotoLight = "RobotoLight";

  static const rubikBlack = "RubikBlack";
  static const rubikBlackItalic = "RubikBlackItalic";
  static const rubikBold = "RubikBold";
  static const rubikBoldItalic = "RubikBoldItalic";
  static const rubikMedium = "RubikMedium";
  static const rubikMediumItalic = "RubikMediumItalic";
  static const rubikRegular = "RubikRegular";
  static const rubikItalic = "RubikItalic";
  static const rubikLight = "RubikLight";
  static const rubikLightItalic = "RubikLightItalic";
  static const rubikSemiBold = "RubikSemiBold";
}
