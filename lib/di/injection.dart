import 'package:catapp/bloc/app_bloc/app_bloc.dart';
import 'package:catapp/bloc/splash_bloc/splash_bloc.dart';
import 'package:catapp/data/provider/local/local_provider.dart';
import 'package:catapp/data/provider/network/network_provider.dart';
import 'package:catapp/data/repository/repository.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void configureInjection() {
  getIt.registerSingleton<LocalProvider>(LocalProvider());
  getIt.registerSingleton<NetworkProvider>(NetworkProvider());
  getIt.registerSingleton<Repository>(Repository());
  getIt.registerSingleton<AppBloc>(AppBloc());
  getIt.pushNewScope(
      scopeName: "splashLevel",
      init: (splashGetIt) {
        splashGetIt.registerSingleton<SplashBloc>(SplashBloc());
      });
}
