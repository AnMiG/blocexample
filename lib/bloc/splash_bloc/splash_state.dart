import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class SplashState extends Equatable {
  @override
  List<Object?> get props => [];
}

class SplashInitialState extends SplashState {}

class SplashFinishedState extends SplashState {
  final bool authorized;

  SplashFinishedState(this.authorized);

  @override
  List<Object?> get props => [authorized];
}
