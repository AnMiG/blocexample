import 'dart:async';
import 'package:catapp/bloc/app_bloc/app_bloc.dart';
import 'package:catapp/bloc/app_bloc/app_state.dart';
import 'package:catapp/bloc/splash_bloc/splash_event.dart';
import 'package:catapp/bloc/splash_bloc/splash_state.dart';
import 'package:catapp/common/utils/logger/logger.dart';
import 'package:catapp/di/injection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  static final String tag = "SplashBloc";

  SplashBloc() : super(SplashInitialState()) {
    log('$tag initialized');
    _subscribeToUserLoading();
  }

  Timer? _timer;
  Duration _splashDuration = Duration(milliseconds: 500);
  late StreamSubscription _streamSubscription;

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    log('$tag mapEventToState $event');

    if (event is SplashTimerFinishedEvent) {
      yield SplashFinishedState(event.authorized);
    }
  }

  void _subscribeToUserLoading() {
    _streamSubscription = getIt.get<AppBloc>().stream.listen((state) {
      log('$tag AppBloc::listen = $state');

      if (state is AuthorizedState) {
        _timer = Timer(_splashDuration, () {
          add(SplashTimerFinishedEvent(true));
        });
      } else if (state is UnauthorizedState) {
        _timer = Timer(_splashDuration, () {
          add(SplashTimerFinishedEvent(false));
        });
      }
    }, onError: (e, stackTrace) {
      log('$tag $e\n$stackTrace');
    });
  }

  @override
  Future<void> close() {
    log('$tag close');
    _timer?.cancel();
    _streamSubscription.cancel();
    return super.close();
  }
}
