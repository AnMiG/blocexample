import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class SplashEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class SplashTimerFinishedEvent extends SplashEvent {
  final bool authorized;

  SplashTimerFinishedEvent(this.authorized);
}
