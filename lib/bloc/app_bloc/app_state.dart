import 'package:catapp/data/model/user.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class AppState extends Equatable {
  @override
  List<Object?> get props => [];
}

class UnauthorizedState extends AppState {}

class AuthorizedState extends AppState {
  final User user;

  AuthorizedState(this.user);

  @override
  List<Object?> get props => [user];
}
