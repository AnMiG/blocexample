import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class AppEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class LoadUserEvent extends AppEvent {}
