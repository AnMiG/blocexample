import 'package:catapp/bloc/app_bloc/app_event.dart';
import 'package:catapp/bloc/app_bloc/app_state.dart';
import 'package:catapp/common/utils/logger/logger.dart';
import 'package:catapp/data/model/user.dart';
import 'package:catapp/data/repository/repository.dart';
import 'package:catapp/di/injection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  static final String tag = "AppBloc";

  AppBloc() : super(UnauthorizedState()) {
    log('$tag initialized');
  }

  final Repository _repository = getIt.get<Repository>();

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    log('$tag mapEventToState $event');

    if (event is LoadUserEvent) {
      yield* _loadUser();
    }
  }

  Stream<AppState> _loadUser() async* {
    User? user = await _repository.getUsser();
    if (user == null) {
      yield UnauthorizedState();
    } else {
      yield AuthorizedState(user);
    }
  }
}
