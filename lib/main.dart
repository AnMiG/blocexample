import 'package:catapp/common/constants.dart';
import 'package:catapp/common/routes/app_router.dart';
import 'package:catapp/common/utils/navigation/navigation.dart';
import 'package:catapp/di/injection.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'common/resources/resources_lib.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  SystemChrome.setEnabledSystemUIOverlays([]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarBrightness: Brightness.light,
    statusBarIconBrightness: Brightness.light,
  ));
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  configureInjection();

  runApp(EasyLocalization(
      supportedLocales: SUPPORTED_LOCALES,
      path: TRANSLATIONS_PATH,
      fallbackLocale: DEFAULT_LOCALE,
      child: App()));
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: AppRouter.onGenerateRoute,
      theme: appTheme(),
      home: AppRouter.getHome(),
      navigatorObservers: [routeObserver],
      navigatorKey: Navigation.navigatorKey,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      debugShowCheckedModeBanner: false,
    );
  }
}
