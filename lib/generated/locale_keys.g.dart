// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const appName = 'appName';
  static const login = 'login';
  static const alertOk = 'alertOk';
  static const alertYes = 'alertYes';
  static const alertNo = 'alertNo';
  static const alertAccept = 'alertAccept';
  static const alertCancel = 'alertCancel';
  static const alertRetry = 'alertRetry';
  static const alertClose = 'alertClose';

}
