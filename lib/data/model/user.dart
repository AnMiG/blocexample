import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String? firstName;
  final String? secondName;
  final String? email;
  final String? picture;

  User({
    this.firstName,
    this.secondName,
    this.email,
    this.picture,
  });

  @override
  List<Object?> get props => throw UnimplementedError();
}
