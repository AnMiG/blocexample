import 'package:catapp/common/utils/logger/logger.dart';
import 'package:catapp/data/model/user.dart';
import 'package:catapp/data/provider/local/local_provider.dart';
import 'package:catapp/data/provider/network/network_provider.dart';
import 'package:catapp/di/injection.dart';

class Repository {
  static final String tag = "Repository";

  final LocalProvider _localProvider = getIt.get<LocalProvider>();
  final NetworkProvider _networkProvider = getIt.get<NetworkProvider>();

  Future<User?> getUsser() {
    log('$tag getUsser');
    return _localProvider.getUsser();
  }
}
